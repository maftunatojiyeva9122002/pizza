/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    // fontFamily: {
    //   Robo:"Roboto Mono,sans-serif",

    // },
    // fontSize:{
    //   '10xl':"150px"
    // },
    extend:{
      fontFamily:{
        pizza:"Roboto mono, monospace"
      },
      colors: {
        pizza: "#FACC15"
      },
      backgroundColor: {
        pizza: "#FACC15"
      },
    },
   
  },
  //*agar extend ni ichida yozsak yangi qo'shiladi ayniki biz berayotgan qiymatga,then ni ichida yozsak o'sha biz yozgan stildan boshqasi ishlamaydi
  plugins: [],
}

