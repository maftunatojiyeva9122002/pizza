import { useSelector } from "react-redux";
import { getTotalCartQuantity, getTotalPriceQuantity } from "./cartSlice";
import { formatCurrency } from "../../utils/helpers";
import { Link } from "react-router-dom";
function CartOverview() {
  const totalCartQuantity = useSelector(getTotalCartQuantity);
  const totalPrice = useSelector(getTotalPriceQuantity);
  if (totalCartQuantity <= 0) return null;
  return (
    <div className="px-3 py-2 bg-stone-900 text-white flex items-center justify-between sm:px-10 sm:py-4 uppercase font-Robo">
      <p className="space-x-4">
        <span className="text-xs sm:text-base">pizzas : {totalCartQuantity} </span>
        <span className="text-xs sm:text-base">Total Price : {formatCurrency(totalPrice)}</span>
      </p>
      <Link className="text-xs sm:text-base" to="/cart">Open cart &rarr;</Link>
    </div>
  );
}

export default CartOverview;
