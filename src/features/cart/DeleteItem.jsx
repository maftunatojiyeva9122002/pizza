import React from "react";
import Button from "../../ui/Button";
import { useDispatch } from "react-redux";
import { deleteCart } from "./cartSlice";


const DeleteItem = ({id}) => {
  const dispatch = useDispatch();

  const handleDeleteItem = (i) => {
    dispatch(deleteCart(i))
  }
  
  return <Button type="small" onClick={()=>handleDeleteItem(id)}>DELETE</Button>;
};

export default DeleteItem;
