import React from 'react'
import Button from '../../ui/Button'
import { useDispatch, useSelector } from 'react-redux'
import { decreaseCart, getCurrentQuantity, increaseCart } from './cartSlice'

const UpdateCartQty = ({id,itemQuantity}) => {
    const {quantity} = useSelector(getCurrentQuantity(id))
    const dispatch = useDispatch()
  
  return (
   <div className=" gap-2 flex  items-center sm:gap-4 ">
    <Button type= "rounded" onClick={()=>dispatch(increaseCart(id))}>+</Button>
    <span>{itemQuantity?.quantity}</span>
   <Button type= "rounded"  onClick={()=>dispatch(decreaseCart(id))}>-</Button>
   </div>
  )
}

export default UpdateCartQty