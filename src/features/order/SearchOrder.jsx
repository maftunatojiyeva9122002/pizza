import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const SearchOrder = () => {
  const [query, setQuery] = useState("");
  const navigate = useNavigate();

  function handleSubmit(e) {
    e.preventDefault();
    if (!query) return;
    navigate(`/order/${query}`);
    setQuery("");
  }
  return (
    <>
      <form onSubmit={handleSubmit}>
        <input
          className="  rounded-full focus:outline-none bg-yellow-50 py-1 px-2 w-32 sm:w-56 sm:px-4  "
          placeholder="Search Order #"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        ></input>
      </form>
    </>
  );
};

export default SearchOrder;
