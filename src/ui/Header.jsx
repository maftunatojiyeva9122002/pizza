import React from "react";
import { Link } from "react-router-dom";
import "../index.css";
import { useSelector } from "react-redux";
import SearchOrder from "../features/order/SearchOrder";

const Header = () => {
  const user = useSelector((state) => state.user.username);
  return (
    <header className=" gap-2 flex sm:gap-6 border-b-1 border-black bg-pizza p-3 font-Robo justify-around items-center ">
      <div className="flex justify-center gap-20">
        <Link
          to="/"
          className=" text-xs sm:text-sm  text-stone-900 uppercase tracking-widest font-bold "
        >
          Fast Pizza Co
        </Link>
      </div>
      <SearchOrder />
      <h1 className=" text-xs py-1 font-bold sm:text-lg uppercase -tracking-tight">
        {user}
      </h1>
    </header>
  );
};

export default Header;
